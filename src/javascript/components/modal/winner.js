import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  // call showModal function
  const root = document.getElementById('root');
  root.innerHTML = '';
  const fragment = createElement({ tagName: 'div', className: 'winner-container' });
  fragment.innerHTML = 'WON';
  const fighterImg = createFighterImage(fighter);
  const fighterName = createElement({ tagName: 'h4', className: 'winner-title' });
  fighterName.innerHTML = fighter.name;
  fragment.append(fighterName, fighterImg);
  root.append(fragment);
}
