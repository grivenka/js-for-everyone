import { createElement } from '../helpers/domHelper';
import { createFightersSelector, getFighterInfo } from './fighterSelector';
import { fighterService } from '../services/fightersService';
import { fighters } from '../helpers/mockData';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  // todo: show fighter info (image, name, health, etc.)
  if (fighter) {
    const fragment = createElement({ tagName: 'div', className: 'fighter-info' });
    const fighterImg = createFighterImage(fighter);
    const fighterName = createElement({ tagName: 'h4', className: 'fighter-info-title' });
    fighterName.innerHTML = fighter.name;
    const fighterHealth = createElement({ tagName: 'p', className: 'fighter-info-health' });
    fighterHealth.innerHTML = `Health: ${fighter.health}`;
    const fighterAttack = createElement({ tagName: 'p', className: 'fighter-info-attack' });
    fighterAttack.innerHTML = `Attack: ${fighter.attack}`;
    const fighterDefense = createElement({ tagName: 'p', className: 'fighter-info-defense' });
    fighterDefense.innerHTML = `Defense: ${fighter.defense}`;
    fragment.append(fighterName, fighterImg, fighterHealth, fighterAttack, fighterDefense);
    fighterElement.append(fragment);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });

  return imgElement;
}
