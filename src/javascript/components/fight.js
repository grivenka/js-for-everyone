import { controls } from '../../constants/controls';
import { showWinnerModal } from './modal/winner';

export async function fight(firstFighter, secondFighter) {
  let currentHealthFirstFighter = firstFighter.health;
  let currentHealthSecondFighter = secondFighter.health;
  let winner;

  document.addEventListener('keydown', function (event) {
    if (event.code === controls.PlayerOneAttack) {
      const damage = getDamage(firstFighter, secondFighter);
      currentHealthSecondFighter -= damage;
      updateHealthIndicator(currentHealthSecondFighter, secondFighter, 'right');
      winner = getAChampion(currentHealthFirstFighter, currentHealthSecondFighter, firstFighter, secondFighter);
    } else if (event.code === controls.PlayerTwoAttack) {
      const damage = getDamage(secondFighter, firstFighter);
      currentHealthFirstFighter -= damage;
      updateHealthIndicator(currentHealthFirstFighter, firstFighter, 'left');
      winner = getAChampion(currentHealthFirstFighter, currentHealthSecondFighter, firstFighter, secondFighter);
    }
  });

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    setTimeout(function () {
      resolve(showWinnerModal(winner));
    }, 10000);
  });
}

function getAChampion(currentHealthFirstFighter, currentHealthSecondFighter, firstFighter, secondFighter) {
  if (currentHealthFirstFighter <= 0) {
    return secondFighter;
  } else if (currentHealthSecondFighter <= 0) {
    return firstFighter;
  }
}

function updateHealthIndicator(currentHealthFighter, fighter, position) {
  const healthBar = document.getElementById(`${position}-fighter-indicator`);

  const indicatorWidth = Math.max(0, (currentHealthFighter * 100) / fighter.health);
  healthBar.style.width = `${indicatorWidth}%`;
}

export function getDamage(attacker, defender) {
  const hitPower = getHitPower(attacker);
  const blockPower = getBlockPower(defender);
  const damage = hitPower - blockPower;
  return damage;
}

export function getHitPower(fighter) {
  const hitPower = fighter.attack;
  // * criticalHitChance( );
  return hitPower;
}

export function getBlockPower(fighter) {
  const blockPower = fighter.defense;
  return blockPower;
}
